struct CatFact: Codable, Identifiable, Hashable {
    let id: String
    let text: String
    let type: String
    var isFavorite: Bool = false

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case text
        case type
    }
}
