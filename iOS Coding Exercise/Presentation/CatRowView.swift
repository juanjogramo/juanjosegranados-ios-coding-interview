//
//  CatRowView.swift
//  iOS Coding Exercise
//
//  Created by Juan José Granados Moreno on 26/03/24.
//

import SwiftUI

struct CatRowView: View {
    var cat: CatFact
    var makeFavoriteAction: () -> Void
    
    var body: some View {
        HStack {
            Text(cat.text)
            Button {
                makeFavoriteAction()
            } label: {
                Image(systemName: "star")
            }
        }
    }
}
