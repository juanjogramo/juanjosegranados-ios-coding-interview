//
//  ContentViewModel.swift
//  iOS Coding Exercise
//
//  Created by Juan José Granados Moreno on 26/03/24.
//

import Foundation

final class ContentViewModel: ObservableObject {
    
    @Published var catFacts: [CatFact] = []
    @Published var selectedCatFact: CatFact? = nil
    @Published var isLoading = false
    
    // MARK: - Fetch Data
    func fetchCats() {
        guard let url = URL(string: "https://cat-fact.herokuapp.com/facts") else { return }
        isLoading = true
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let data = data {
                do {
                    let catFacts = try JSONDecoder().decode([CatFact].self, from: data)
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.catFacts = catFacts
                    }
                } catch {
                    print("Error decoding JSON: \(error)")
                }
            } else if let error = error {
                print("Error fetching data: \(error)")
            }
        }
        .resume()
    }
    
    func updateFavorite() {
        selectedCatFact?.isFavorite.toggle()
    }
    
}

