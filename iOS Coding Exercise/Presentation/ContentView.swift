import SwiftUI

struct ContentView: View {
    // MARK: - Properties
    @StateObject var viewModel = ContentViewModel()
    
    // MARK: - Body
    var body: some View {
        NavigationStack {
            List($viewModel.catFacts) { $cat in
                NavigationLink(value: cat) {
                    CatRowView(cat: cat) {
                        viewModel.selectedCatFact = cat
                        viewModel.updateFavorite()
                    }
                }
            }
            .refreshable {
                viewModel.fetchCats()
            }
            .navigationDestination(for: CatFact.self) { cat in
                CatFactDetailView(catFact: cat)
            }
            .navigationTitle("Cat Facts")
            .onAppear {
                viewModel.fetchCats()
            }
            .toolbar {
                ToolbarItem(placement: .navigation) {
                    if viewModel.isLoading {
                        ProgressView()
                    } else {
                        Button("Refresh") {
                            viewModel.fetchCats()
                        }
                    }
                    
                }
            }
        }
    }
    
    
    
}

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
