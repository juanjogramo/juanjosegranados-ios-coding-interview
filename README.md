# BEON iOS Coding Exercise

This is a simple iOS app based on a public API.

## Before the live coding challenge
This project was created using XCode 15.3. 
Make sure you can run the project on an emulator before the day of the live coding.
Familiarize yourself with the patterns used in the codebase as you will have to work on them.

## During the live coding exercise
Before joining the session, make sure that your IDE and emulator are ready in order to see any changes you make live.
We will provide you with a series of tasks. Some of them will be simple improvements, some others will be features that we'd like to see implemented.
Try and pick those that you think will be able to finish in the provided time.